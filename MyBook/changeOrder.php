<?php
    require("lib/database.php");
    error_reporting(E_ALL);

    $db = new Database();
    session_start();


    if (isset($_GET['idOrder']) && isset($_GET['Stato'])) {

        if($_GET['Stato']=='Spedito' || $_GET['Stato']=='Annullato'){
            $ordineId = $_GET['idOrder'];
            $stato = $_GET['Stato'];

            $query = "UPDATE Ordine SET Stato='{$stato}' WHERE Ordine.ordineID = ?";

            $db->newQuery($query);
            $db->bindInQuery("s",$ordineId);
            $db->executeQuery();

            $db->newNotify($_SESSION['utenteID'], $db->getOrder($ordineId)[0]['utenteID'], "Aggiornamento ordine", "Il tuo ordine ".$ordineId." e' stato ".$stato.".");
        }

        if($_GET['Stato']=='Eliminato'){
            $ordineId = $_GET['idOrder'];

            $query = "DELETE FROM Ordine WHERE ordineID = ?";

            $db->newQuery($query);
            $db->bindInQuery("s",$ordineId);
            $db->executeQuery();

            $db->newNotify($_SESSION['utenteID'], $db->getOrder($ordineId)[0]['utenteID'], "Aggiornamento ordine", "Il tuo ordine ".$ordineId." precendentemente annullato è stato eliminato dal server ");
        }
    }

    header("location:vendorPage.php");

?>