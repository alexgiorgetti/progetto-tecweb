function hideElement(element){
    element
        .removeClass("selected")
        .next().slideUp();
}

$(document).ready(function(){

    $("div.accordion button")
        .next().hide();

    $("div.accordion button").click(function(){
        if($(this).hasClass("selected")){
            hideElement($(this));
        }
        else{
            $(this)
                .addClass("selected")
                .next().slideDown();
        }
    });

    $("button.close-all").click(function(){
        $('.selected').each(function() {
            hideElement($(this));
          });
    });
    
});

