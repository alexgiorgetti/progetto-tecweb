<?php 
    require("bootstrap.php");
    $index_modal = 0;

    if(isset($_SESSION['search']) && $_SESSION['search'] == 'ok'){
        $index_modal = 1;
    }

    $libri = $db->allBooks();
?>

<!DOCTYPE html>

<html lang="en">
    <head>
        <title>Homepage</title>
        <link href="class/css/index.css" rel="stylesheet">
        <?php include 'head.php' ?>
        <?php include 'navbar.php' ?>
        <script>
            $(document).ready(function() {
                var modal = <?php echo $index_modal; ?>;
                if(modal == 0){
                    $('#search').hide();
                    $('#allbooks').show();
                }

                $('#cerca').click(function() {

                    titolo = $("#titolo").val();
                    autore = $("#autore").val();

                    if(titolo != "" || autore != "Autore"){
                        $.ajax({
                            type: "GET",
                            url: "search.php",
                            data: "titolo=" + titolo + "&autore=" + autore,
                            dataType: "html",
                            success: function(response)
                            {
                                $("#search-result").html(response); 
                            },
                            error: function(){}
                        });

                        $('#search').show();
                        $('#allbooks').hide();
                    }
                });

                $('#div_gen > button').click(function(e) {
                    $.ajax({
                        type: "GET",
                        url: "search.php",
                        data: "genere=" + this.id,
                        dataType: "html",
                        success: function(response)
                        {
                            $("#search-result").html(response); 
                        },
                        error: function(){}
                    });

                    $('#search').show();
                    $('#allbooks').hide();                
                });

                $('#reset-search').click(function() {
                    $('#search').hide();
                    $('#allbooks').show();
                });

            });
        </script>

    </head>
    
    <body>

        <section class="page-section clearfix">
            <div class="container" style="padding-top:20px">
                <div class="text-center bg-faded p-3 rounded">
                    <table class="table table-fit table-borderless">
                        <tbody>
                            <tr>
                                <td style="padding-top:5%">
                                    <img src="img/logo.png" />
                                </td>
                                <td class="pl-0" style="padding-top:5%">
                                    <h3> Benvenuto ! </h3>
                                    <p class="mb-3"><i>Qui troverai una vasta gamma di libri di vario genere e di diversi autori. <br>Ricordati di creare un account se ancora non l'hai fatto!</i></p>
                                    
                                    <?php
                                        if(isset($_SESSION['Login']) && $_SESSION['Login']){
                                            echo '<a class="btn btn-outline-primary" href="userPage.php">Account</a>';
                                        } else
                                            echo '<a class="btn btn-outline-primary" href="login.php#SignUp">Registrati</a>';
                                    ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>

        <div class="box" id="search-box">
            <div class= "container rounded border border-warning mb-2 my-4 p-4">

                <div class="row justify-content-center">
                    <h4 style="padding:1%">
                        <strong>Cerca Libri</strong>
                    </h4>
                </div>

                <form id="search-form">
                    <div class = "row justify-content-center">

                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control" id="titolo" placeholder="Titolo">
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <select class="form-control" id="autore">
                                <option>Autore</option>
                                <?php 
                                    $autori = $db->checkAuthors();
                                    foreach ($autori as $key => $value) {
                                        echo "<option value='".$value['autoreID']."'>".$value['Nome']." ".$value['Cognome']."</option>";
                                    }
                                ?>
                                </select>
                            </div>
                        </div>
                        
                        <div class="col-md-4">
                            <button type="button" class="btn btn-outline-primary" id="cerca" >Cerca</button>
                            <input type="reset" class="btn btn-outline-primary" value="Reset"/>
                        </div>
                        
                    </div>
                </form>

            </div>
            </div> 
        </div>

        <div class="box" id="gen">
            <div class="container rounded border border-warning my-5 p-5">

                <div class="row justify-content-center">
                    <h4 style="padding:1%">
                        <strong>Ricerca per Genere</strong>
                    </h4>
                </div>

                <div class="row justify-content-center" id="div_gen">
                    <?php 
                        $gen = $db->checkGen();
                        $tmpArray = array();
    
                        foreach ($gen as $key => $value) {
                            if(!in_array($value['Genere'], $tmpArray))
                                echo '<button type="button" class="btn btn-outline-primary" id="'.$value['Genere'].'" style="margin:10px;" data-icon="home">'.$value['Genere'].'</button>';
                                $tmpArray[] = $value['Genere'];
                        }
                    ?>
                </div>

            </div>
        </div>

        <div class="box" id="search">
            <div class="container rounded border border-warning mb-2 my-5 p-5">

                <div class="row justify-content-center">
                    <h4 style="padding:1%">
                        <strong>Risultato ricerca</strong>
                    </h4>
                </div>

                <div class="row justify-content-center">
                    <div id="search-result">
                    </div>
                </div>

                <div class="row justify-content-center">
                <input type="reset" class="btn btn-outline-primary" id="reset-search" value="Rimuovi ricerca"/>
                </div>

            </div>
        </div>

        <div class="box" id="allbooks">
            <div class="container rounded border border-warning mb-2 my-5 p-5">

                <div class="row justify-content-center">
                    <h4 style="padding:1%">
                        <strong>Bacheca Libri</strong>
                    </h4>
                </div>
                
                <div class="row justify-content-center">
                    <?php
                        echo '<div class="card-deck mb-2 text-center" style="padding:30px;">';
                        foreach($libri as $key => $value){

                            $author = $db->authorOfBook($value['autoreID']);
                            $seller = $db->sellerOfBook($value['venditoreID']);
                            $editor = $db->editorOfBook($value['casaEditriceID']);

                            include 'libri.php';
                        }
                        echo '</div>'
                    ?>
                </div>

            </div>
        </div>

        <?php include 'footer.html' ?>
    </body>
</html>