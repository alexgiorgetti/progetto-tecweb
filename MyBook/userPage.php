<?php
    require("bootstrap.php");
    error_reporting(E_ALL);

    $modal = 0;

    if (!isset($_SESSION['Login']) || !$_SESSION['Login']){
        header("location:login.php");
    } else if ($_SESSION['Ruolo'] == 2){
        $modal = 1;
    }

    if(isset($_GET['view'])){
        $showOrder = $_GET['view'];
        $book = json_decode($db->getOrder($showOrder)[0]['LibroOrdinato']);
    } else {
        $showOrder = -1;
        $book = array();
    }

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="stylesheet"  href="class/css/user_form.css ">
        <?php include 'head.php' ?>
        <?php include 'navbar.php'?>
        <title>Utente</title>

        <script>
            $(document).ready(function() {
                $('#orderDiv').hide();
                var modal = <?php echo $modal; ?>;
                
                if(modal == 1){
                    $('#orderDiv').hide();
                } else {
                    $('#orderDiv').show();
                    var showOrder = <?php echo $showOrder; ?>;
                    if(showOrder != -1) {
                        <?php
                            
                            echo "$('#modalText').append('Ordine ".$showOrder.":<br />');";
                            
                            foreach($book as $key => $value) {
                                echo "$('#modalText').append('Libro: ".$db->getBookbyId($key)[0]['Titolo']." - Quantità: ".$value."<br />');";
                            }
                            
                        ?>
                        $('#userModal').modal('toggle');
                        $('#userModal').modal('show');
                    }
                }
            });
        </script>

    </head>

    <body>

        <!-- Modal -->
        <div class="modal fade" id="userModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="userModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title title-imp" id="userModalLabel">Attenzione</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body text-write">
                        <p id="modalText"></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-warning" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="box">
            <div class= "container rounded border border-warning mb-2 my-4 p-4 bg-faded ">

                <div class="row justify-content-center ">
                    <h1 style="padding:1%">
                        <strong>Benvenuto <?php echo $_SESSION['Nome'];?>!</strong>
                    </h1>
                </div>

                <div class="box">
                    <div id="orderDiv" class= "container rounded border border-warning mb-2 my-4 p-4" style="overflow-x:auto;">    
                        <div class="row justify-content-center">
                            <h2  style="padding:1%">
                                <strong>Stato Ordini</strong>
                            </h2>
                        </div> 

                        <table id="orderTable" class="table text-center">
                            <thead>
                                <tr>
                                    <th scope="col">ID Ordine</th>
                                    <th scope="col">Data</th>
                                    <th scope="col">Stato</th>
                                    <th scope="col">Azione</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                    $ord = $db->ordineUtente($_SESSION['utenteID']); 

                                    foreach ($ord as $ordine) {
                                        
                                        $ordineId = $ordine['ordineID'];
                                        $libro = $ordine['LibroOrdinato'];
                                        $dataOrdine = $ordine['DataOrdine'];
                                        $stato = $ordine['Stato'];   

                                        echo "<tr>";

                                        echo "<td><a href='userPage.php?view={$ordineId}'>{$ordineId}</a></td>";
                                        echo "<td>{$dataOrdine}</td>"; 
                                        echo "<td>{$stato}</td>"; 
                                        if ($stato == 'Spedito'){
                                            echo "<td><a class='btn btn-success'> <span class='fa fa-flag'></span></a></td>";
                                        }
                                        else if ($stato != 'Annullato'){
                                            echo "<td><a class='btn btn-danger' href='userPage.php?cancelOrder={$ordineId}'><strong class='fa fa-times'></strong></a></td>";
                                        } else { }

                                        echo "</tr>";
                                    }

                                    if (isset($_GET['cancelOrder'])) {
                                        $ordineId = $_GET['cancelOrder'];

                                        $query = "UPDATE Ordine SET Stato='Annullato' WHERE Ordine.ordineID = ?";

                                        $db->newQuery($query);
                                        $db->bindInQuery("s",$ordineId);
                                        $db->executeQuery();

                                        $db->newNotify($_SESSION['utenteID'], $db->getOrder($_GET['cancelOrder'])[0]['venditoreID'], "Modifica stato ordine", "Un utente ha eliminato il suo ordine ".$_GET['cancelOrder']);

                                        header("Location:userPage.php");
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>

                    <div class="box">
                        <div class= "container rounded border border-warning mb-2 my-4 p-4">
                            <div class="row justify-content-center">
                                <h2  style="padding:1%">
                                    <strong>Dati Personali</strong>
                                </h2>
                            </div> 
                            
                            <form id="user-form" method="post" action="userPage.php" role="form">
                                <?php
                                    if (isset($_POST['salva'])){
                                        echo '<script type=text/javascript>';

                                        $updateName=$_POST['name'];
                                        $updateSurname=$_POST['surname'];
                                        $updateDate=$_POST['date'];
                                        $updatePhone=$_POST['phone'];
                                        $updateAdress=$_POST['adress'];
                                        $updateCity=$_POST['city'];

                                        $update="UPDATE Utente 
                                        SET Nome='".$updateName."',Cognome='".$updateSurname."',DataNascita='".$updateDate."',Telefono='".$updatePhone."',IndirizzoResidenza='".$updateAdress."',CittaResidenza='".$updateCity."'
                                        WHERE utenteID = ?";

                                        $db->newQuery($update);   
                                        $db->bindInQuery("s",$_SESSION['utenteID']);
                                        $success = $db->executeQuery();
                                
                                        $_SESSION['Nome']=$updateName;
                                        $_SESSION['Cognome']=$updateSurname;
                                        $_SESSION['DataNascita']=$updateDate;
                                        $_SESSION['Telefono']=$updatePhone;
                                        $_SESSION['IndirizzoResidenza']=$updateAdress;
                                        $_SESSION['CittaResidenza']=$updateCity;

                                        if ($success){
                                            echo '$("#modalText").text("Dati aggiornati con successo");';
                                        }
                                        else {
                                            echo '$("#modalText").text("Dati non aggiornati");';
                                        }

                                        echo "$('#userModal').modal('show'); </script>";
                                    }
                                ?>
                                
                                <div class="controls">

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group text-center">
                                                <label for="nome" class="text-center">Nome</label>
                                                <input id="nome" type="text" type="text" name="name" class="form-control" value="<?php echo $_SESSION['Nome']; ?>" required="required" data-error="Nome richiesto.">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group text-center">
                                                <label for="cognome">Cognome</label>
                                                <input id="cognome" type="text" type="text" name="surname" class="form-control" value="<?php echo $_SESSION['Cognome']; ?>" required="required" data-error="Cognome richiesto">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group text-center">
                                                <label for="email">Email</label>
                                                <input id="email" type="text" type="email" name="mail" class="form-control" value="<?php echo $_SESSION['Username']; ?>" required="required" data-error="Mail valida richiesta.">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class = "row">
                                        <div class="col-md-6">
                                            <div class="form-group text-center">
                                                <label for="telefono">Telefono</label>
                                                <input id="telefono" type="number" name="phone" class="form-control" value="<?php echo $_SESSION['Telefono']; ?>" required="required" data-error="Inserisci un numero di telefono valido.">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group text-center">
                                                <label for="data">Data di nascita</label>
                                                <input id="data" type="date" name="date" class="form-control" value="<?php echo $_SESSION['DataNascita'];?>" required="required" data-error="Inserisci una data valida.">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group text-center">
                                                <label for="indirizzo" class="text-center">Indirizzo di residenza</label>
                                                <input id="indirizzo" type="text" name="adress" class="form-control" value="<?php echo $_SESSION['IndirizzoResidenza'];?>"  required="required" data-error="Inserisci un indirizzo.">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group text-center">
                                                <label for="citta">Citta di residenza</label>
                                                <input id="citta" type="text" name="city" class="form-control" value="<?php echo $_SESSION['CittaResidenza'];?>" required="required" data-error="Inserisci una citta.">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        
                                    </div>

                                    <div class = "row">
                                        <div class="col text-center">
                                            <input type="submit" id="save" name="salva" value="Modifica Dati" class="btn btn-success">
                                        </div>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="box">
                        <div class= "container rounded border border-warning mb-2 my-4 p-4">
                            <div class="row justify-content-center">
                                <h2  style="padding:2%">
                                    <strong>Modifica Password</strong>
                                </h2>
                            </div>

                            <form id="changepw-form" method="POST" action="userPage.php">
                                <?php

                                if (isset($_POST['changePsw'])){
                                    echo '<script type=text/javascript>';
                                    $currentPassword = $_POST['oldPsw'];
                                    $newPassword = $_POST['newPsw'];
                                    $confirmPassword = $_POST['newPswRpt'];

                                    $getPsw = "SELECT * FROM Utente WHERE utenteID = ?";

                                    $db->newQuery($getPsw);
                                    $db->bindInQuery("s",$_SESSION['utenteID']);
                                    $password = $db->resultQuery()[0];

                                    $password = $password['Password'];

                                    if(password_verify($currentPassword,$password)){
                                        if($newPassword == $confirmPassword){
                                            $updatePsw= "UPDATE Utente SET Password= '". password_hash($newPassword,PASSWORD_DEFAULT) ."' WHERE utenteID = ?";

                                            $db->newQuery($updatePsw);
                                            $db->bindInQuery("s",$_SESSION['utenteID']); 
                                            $db->executeQuery();

                                            echo '$("#modalText").text("Password cambiata correttamente");';
                                        }
                                        else{
                                            echo '$("#modalText").text("Le due passowrd non corrispondono");';
                                        }
                                    }
                                    else{
                                        echo '$("#modalText").text("Password vecchia non corretta");';
                                    }  

                                    echo "$('#userModal').modal('show'); </script>";
                                }
                                ?>

                                <div class = "row justify-content-center">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label style="display:none;" for="vecchiaPassword">Vecchia Password</label>
                                            <input type="password" class="form-control" id="vecchiaPassword" name="oldPsw" placeholder="Vecchia Password">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label style="display:none;" for="nuovaPassword">Nuova Password</label>
                                            <input type="password" class="form-control" id="nuovaPassword" name="newPsw" placeholder="Nuova Password">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label style="display:none;" for="confermaPassword">Conferma Password</label>
                                            <input type="password" class="form-control" id="confermaPassword" name="newPswRpt" placeholder="Ripeti Nuova Password">
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-3">
                                        <button type="submit" class="btn btn-primary btn-block" id="cambiaPassword" name="changePsw" ><span class="fa fa-lock fa-lg"></span> Modifica</button>
                                    </div>          
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <?php include 'footer.html' ?>

    </body>

</html>