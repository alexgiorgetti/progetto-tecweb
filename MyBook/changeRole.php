<?php
    require("lib/database.php");
    error_reporting(E_ALL);

    $db = new Database();
    session_start();

    if (isset($_POST['id']) && isset($_POST['role'])){
        $aggiornaRuolo=$_POST['role'];
        $utenteId = $_POST['id'];

        $query="UPDATE Utente SET Ruolo = '{$aggiornaRuolo}' WHERE utenteID = ?";

        $db->newQuery($query);
        $db->bindInQuery("s",$utenteId);
        $db->executeQuery();

        header("location:adminPage.php");

    }

?>