<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta https-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap Core CSS -->
<link href="./utility/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet"  href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
<link rel="icon" href="img/icon.ico">

<!-- jQuery -->
<script src="./utility/jquery/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="./utility/bootstrap/js/bootstrap.min.js"></script>