<?php
    require("bootstrap.php");

    function checkSignUpValue() {
        if (!(empty($_GET['name']) OR empty($_GET['surname']) OR empty($_GET['username']) 
            OR empty($_GET['password']) OR empty($_GET['confpassword']) OR empty($_GET['phone']) OR empty($_GET['bdate']) 
            OR empty($_GET['city']) OR empty($_GET['address']))){
            return true;
        }

        return false;
    }

    function checkSignInValue() {
        if (!(empty($_POST['username_login']) OR empty($_POST['password_login']))){
            return true;
        }

        return false;
    }

    function checkPhone() {
        if (strlen($_GET['phone']) == 10){
            return true;
        }

        return false;
    }

    function checkDateV() {
        if(strtotime($_GET['bdate']) > strtotime('now')){
            return false;
        }

        return true;
    }

    if (checkSignUpValue()){

        if(!checkPhone()){
            $_SESSION['signUpError'] = "phone";
            header("location:login.php#SignUp");
            return;
        }
        if(!checkDateV()){
            $_SESSION['signUpError'] = "bdate";
            header("location:login.php#SignUp");
            return;
        }
        if ($_GET['password'] != $_GET['confpassword']){
            $_SESSION['signUpError'] = "pass";
            header("location:login.php#SignUp");
            return;
        } 

        /* Prendiamo un'eventuale riga nel DB con la stessa mail per controllare 
            che non sia già registrato un utente con la stessa mail */
        $email = $_GET['username'];
        $query =   "SELECT *
                    FROM Utente
                    WHERE Username = ? LIMIT 1";

        $db->newQuery($query);
        $db->bindInQuery("s", $email);
        $rows= $db->resultQuery();

        /* Se non esiste gia l'utente registrato */
        if(!($rows)){

            $password = $_GET['password'];
            //Crypt della password
            $hashedPassword = password_hash($password, PASSWORD_DEFAULT);

            $db->newUser($_GET['name'], $_GET['surname'], $email, $hashedPassword, $_GET['phone'], 
                            $_GET['bdate'], $_GET['address'], $_GET['city'], 0);

            $_SESSION['registrazione'] = "ok";
            header("location:login.php#SingIn");
        }else {
            $_SESSION['signUpError'] = "email";
            header("location:login.php#SignUp");
        }
        
    } else if (checkSignInValue()){
        $query = "SELECT *
                FROM Utente 
                WHERE Username = ? LIMIT 1";

        $db->newQuery($query);
        $db->bindInQuery("s", $_POST['username_login']);
        $rows=$db->resultQuery();

        if(($rows) && password_verify($_POST['password_login'], $rows[0]["Password"])){
            $_SESSION['Ruolo'] = $rows[0]["Ruolo"];
            $_SESSION['utenteID'] = $rows[0]["utenteID"];
            $_SESSION['Username'] = $rows[0]["Username"]; 
            $_SESSION['Nome'] = $rows[0]["Nome"];
            $_SESSION['Cognome'] = $rows[0]["Cognome"];
            $_SESSION['Login'] = True;
            $_SESSION['Notifiche'] = $db->getNotify($_SESSION['utenteID']);
            $_SESSION['Telefono'] = $rows[0]["Telefono"];
            $_SESSION['DataNascita'] = $rows[0]["DataNascita"];	
            $_SESSION['IndirizzoResidenza'] = $rows[0]["IndirizzoResidenza"];
            $_SESSION['CittaResidenza'] = $rows[0]["CittaResidenza"];
            header("location:index.php");
       }else {
            $_SESSION['userError'] = "true";
            header("location:login.php");
       }
    }else {
        $_SESSION['Login'] = False;
        header("location:index.php");
    }

?>