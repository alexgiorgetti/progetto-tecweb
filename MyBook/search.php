<?php 
    require("bootstrap.php");

    if(isset($_GET['autore']) || isset($_GET['titolo'])){

        $libri = $db->searchBook($_GET['titolo'], $_GET['autore']);
        
        if($libri != null){
            echo '<div class="card-deck mb-3 text-center" style="padding:30px;">';

            foreach($libri as $key => $value){

                $author = $db->authorOfBook($value['autoreID']);
                $seller = $db->sellerOfBook($value['venditoreID']);
                $editor = $db->editorOfBook($value['casaEditriceID']);

                include 'libri.php';
            }
        }
        else{
            echo "<p class='text-center'><i>Non sono stati trovati libri</i></p>";;
        }
    }

    if(isset($_GET['genere'])){
        $libriGen = $db->bookByGen($_GET['genere']);

        if($libriGen != null){
            echo '<div class="card-deck mb-3 text-center" style="padding:30px;">';

            foreach($libriGen as $key => $value){

                $author = $db->authorOfBook($value['autoreID']);
                $seller = $db->sellerOfBook($value['venditoreID']);
                $editor = $db->editorOfBook($value['casaEditriceID']);

                include 'libri.php';
            }
        }
        else{
            echo "<p class='text-center'><i>Non sono stati trovati libri con il genere selezionato</i></p>";;
        }
    }
?>