<?php
    require_once 'bootstrap.php';

    if(!isset($_SESSION['Login']) || !isset($_POST["confirm"]) || empty($_SESSION['cart'])){
        header("location: index.php");
    }

    $templateParams["titolo"] = "Riepilogo Ordine";
    $templateParams["nome"] = "orderDone.php";

    $date = new DateTime();

    $libriOrdinati = json_encode($_SESSION["cart"]);

    foreach($_SESSION['cart'] as $key => $value){
        $sellerBookC = $db->getBookbyId($key)[0]['venditoreID'];
        break;
    }

    $utenteID = $_SESSION["utenteID"];
    $dataOrdine = date("Y-m-d");
    $stato = "Confermato";
    $metodoPagamento = $_POST['need'];

    $db->newOrder($utenteID, $dataOrdine, $stato, $libriOrdinati, $sellerBookC);
    $db->newNotify(-2, $_SESSION['utenteID'], "Conferma nuovo ordine", "Il tuo ordine in data ".$dataOrdine." e' stato processato correttamente.");
    $db->newNotify(-2, $sellerBookC, "Conferma nuovo ordine", "Hai ricevuto un ordine in data ".$dataOrdine.".");

    require 'template/base.php';

?>