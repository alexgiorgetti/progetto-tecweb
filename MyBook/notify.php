<?php
    require_once 'bootstrap.php';
    //Base Template
    $templateParams["titolo"] = "Notifiche";
    $templateParams["nome"] = "notifypage.php";

    if(!isset($_SESSION['Login']) || $_SESSION['Login']!=True)
        header("location:index.php");
    
    require_once 'template/base.php';

?>