<?php
    require("bootstrap.php");
    $login_modal = 0;

    if(isset($_SESSION['Login']) && $_SESSION['Login'] == True){
        header("location:userPage.php");
    }
    
    if(isset($_SESSION['userError']) && $_SESSION['userError'] == "true"){
        $login_modal = 1;
        unset($_SESSION['userError']);
    }
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="stylesheet" href="class/css/login_form.css">
        <?php include 'head.php' ?>
        <?php include 'navbar.php' ?>
        <title>Login</title>
        <script>
            $(document).ready(function(){
                var modal = <?php echo $login_modal; ?>;
                var url = window.location.href;

                if(modal == 1){
                    $('#loginwrong').modal('toggle');
                    $('#loginwrong').modal('show');
                }

                if(url == "http://localhost/MyBook/login.php#SignUp"){
                    $("#sign-in-form").hide();
                    $("#sign-up-form").show();
                }

                $("#form-sign-in").click(function() {
                    $("#sign-in-form").hide();
                    $("#sign-up-form").show();
                });

                $("#form-sign-up").click(function() {
                    $("#sign-in-form").show();
                    $("#sign-up-form").hide();
                });

            });
        </script>
    </head>
    <body>

        <!-- Modal -->
        <div class="modal fade" id="loginwrong" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="loginwrongLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title title-imp" id="loginwrongLabel">Attenzione</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body text-write">
                        Controlla i dati di login che hai inserito e riprova..
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-warning" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <!--- General Page -->
        <section id="cover" >
            <div id="cover-caption">
                <div class="container">
                    <div class="row text-white">
                        <div class="col-xl-5 col-lg-6 col-md-10 col-sm-10 mx-auto text-center form p-4">

                            <!-- Sing-In Form -->
                            <form id="sign-in-form" role="form" method="POST" action="checkLogin.php" <?php if (isset($_SESSION['signUpError'])){ echo "style='display: none;'"; }else{ echo "style='display: block;'"; } ?>>

                                <h1 class="display-4 py-2 text-truncate">Login.</h1>
                                <div class="px-2">

                                    <?php 
                                        if (isset($_SESSION['registrazione']) && ($_SESSION['registrazione'] == "ok")){
                                            echo ("<p class='btn-outline-success text-center text-write'>Registrazione effettuata!</p>");
                                        }

                                        unset($_SESSION['registrazione']);
                                    ?>

                                    <div class="form-group">
                                        <label style="display:none;" for="username_login">Email</label>
                                        <input name="username_login" id="username_login" type="text" class="form-control" placeholder="Email" style="border-bottom:1px solid #434a52" required>
                                    </div>
                                    <div class="form-group">
                                        <label style="display:none;" for="password_login">Password</label>
                                        <input name="password_login" id="password_login" type="password" class="form-control" placeholder="Password" style="border-bottom:1px solid #434a52" required>
                                    </div>
                                    <div>
                                        <button type="submit" class="btn btn-outline-light">Login</button>
                                    </div>
                                    <br>
                                    <p class="text-write">Se non sei ancora registrato? <a id="form-sign-in" class="text-decoration-none" href="#">Iscriviti</a></p>
                                    
                                </div>
                            </form>


                            <!-- Sign-Up Form -->
                            <form id="sign-up-form" role="form" action="checkLogin.php" <?php if (isset($_SESSION['signUpError'])){ echo "style='display: block;'"; }else{ echo "style='display: none;'"; } ?>>
                                
                                <h1 class="display-4 py-2 text-truncate">Iscriviti.</h1>
                                <div class="px-2">

                                    <?php
                                        if(isset($_SESSION['signUpError']) && $_SESSION['signUpError'] == "email"){
                                            echo ("<p class='btn-outline-danger text-center text-write'>Email gia' registrata!</p>");
                                        }
                                        if(isset($_SESSION['signUpError']) && $_SESSION['signUpError'] == "pass"){
                                            echo ("<p class='btn-outline-danger text-center text-write'>Password non combaciano!</p>");
                                        }
                                        if(isset($_SESSION['signUpError']) && $_SESSION['signUpError'] == "phone"){
                                            echo ("<p class='btn-outline-danger text-center text-write'>Telefono errato!</p>");
                                        }
                                        if(isset($_SESSION['signUpError']) && $_SESSION['signUpError'] == "bdate"){
                                            echo ("<p class='btn-outline-danger text-center text-write'>Data di nascita errata!</p>");
                                        }

                                        unset($_SESSION['signUpError']);
                                    ?>

                                    <div class="form-group">
                                        <label style="display:none;" for="name">Nome</label>
                                        <input name="name" id="name" type="text" class="form-control" value="" placeholder="Nome" required>
                                    </div>
                                    <div class="form-group">
                                        <label style="display:none;" for="surname">Cognome</label>
                                        <input name="surname" id="surname" type="text" class="form-control" value="" placeholder="Cognome" required>
                                    </div>
                                    <div class="form-group">
                                        <label style="display:none;" for="username">Email</label>
                                        <input name="username" id="username" type="email" class="form-control" value="" placeholder="Email" required>
                                    </div>
                                    <div class="form-group">
                                        <label style="display:none;" for="password">Password</label>
                                        <input name="password" id="password" type="password" class="form-control" value="" placeholder="Password" required>
                                    </div>
                                    <div class="form-group">
                                        <label style="display:none;" for="confpassword">Conferma Password</label>
                                        <input name="confpassword" id="confpassword" type="password" class="form-control" value="" placeholder="Conferma Password" required>
                                    </div>
                                    <div class="form-group">
                                        <label style="display:none;" for="phone">Telefono</label>
                                        <input name="phone" id="phone" type="number" class="form-control" value="" placeholder="Telefono" required>
                                    </div>
                                    <div class="form-group">
                                        <label style="display:none;" for="bdate">Data di nascita</label>
                                        <input name="bdate" id="bdate" type="date" min="1997-01-01" max="2030-12-31" class="form-control" value="" required>
                                    </div>
                                    <div class="form-group">
                                        <label style="display:none;" for="city">Città residenza</label>
                                        <input name="city" id="city" type="text" class="form-control" value="" placeholder="Città" required>
                                    </div>
                                    <div class="form-group">
                                        <label style="display:none;" for="address">Indirizzo residenza</label>
                                        <input name="address" id="address" type="text" class="form-control" value="" placeholder="Indirizzo" required>
                                    </div>
                                    <div>
                                        <button type="submit" class="btn btn-outline-light">Registrati</button>
                                    </div>
                                    </br>
                                    
                                    <p class="text-write">Torna al <a id="form-sign-up" class="text-decoration-none" href="#">Login</a></p>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
            
        <?php include 'footer.html' ?>
    </body>
</html>