<?php
    require("lib/database.php");
    error_reporting(E_ALL);

    $db = new Database();
    session_start();
    if (isset($_GET['editAvailability']) && ($_GET['editAvailable'])) {
        $libroId = $_GET['editAvailability'];
        $disponibilita = $_GET['editAvailable'];

        if ($disponibilita=='NO'){
            $disponibile=0;
        }
        else if($disponibilita=='SI'){
            $disponibile=1;
        }

        $query = "UPDATE Libro SET Disponibile={$disponibile} WHERE libroID = ?";

        $db->newQuery($query);
        $db->bindInQuery("s",$libroId);
        $db->executeQuery();

        header("location:vendorPage.php");

    }
?>