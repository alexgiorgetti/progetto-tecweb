<?php
    require("lib/database.php");
    $db = new Database();

    session_start();

    if(($_GET) && isset($_GET['id'])){

        if(isset($_SESSION['cart'])){

            if (array_key_exists($_GET['id'], $_SESSION['cart'])){
                #se c'è aggiorniamo
                $_SESSION['cart'][$_GET['id']] = $_SESSION['cart'][$_GET['id']] + $_GET['quantity'];
            }else {
                #se non c'è aggiungiamo
                $_SESSION['cart'][$_GET['id']] = $_GET['quantity'];
            }

        }else {
            $_SESSION['cart'] = array($_GET['id'] => intval($_GET['quantity']));
        }

        header("location:index.php");
    }
?>