<div class= "container rounded border border-warning mb-8 my-4 p-4">
        <header class="section-header text-center">
            <h5 class="text-center"> Ordine effettuato con successo </h5>
            <h6 class="text-center"> Controlla la pagina utente per l'ID </h6>
        </header>
        
        <?php foreach($_SESSION['cart'] as $key => $value): ?>
                <?php
                    $book = $db->getBookbyId($key)[0]; 
                ?>
                <hr class="mt-2 mb-3"/>
                <div class="row">
                    <div class="col-md-3">
                        <img class="img w-25 pl-3" src="<?php echo $book["Immagine"]; ?>" alt="" />
                    </div>
                    <div class="col-md-5"> 
                        <h5><?php echo $book["Titolo"]; ?></h5>
                    </div>
                    <div class="col-md-3">
                        <label><?php echo $value; ?></label>
                    </div>
                    <div class="col-md-1 center-text">
                        <strong><?php echo $book["Prezzo"]."€"; ?></strong>
                    </div>
                </div>
            <?php endforeach; ?>
    </div>

    <p class="text-center">Verrai reinderizzato alla home in 10 secondi.</p>

    <?php
        unset($_SESSION['cart']);
        header("refresh:10;url=index.php"); 
    ?>
