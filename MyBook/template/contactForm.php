<section id="cover">
    <div id="cover-caption">
        <div class="container">
            <div class="row text-white">
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-10 mx-auto text-center form p-4">
                    <h1 class="text-center">Contattaci.</h1>
                    <p class="lead">Per qualsiasi domanda siamo a tua completa disposizione!<br> Ti basterà compilare il form che segue.</p>  
                    <form id="contact-form" method="post" action="sendMessage.php">
                        <div class="controls">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="form_name">Nome *</label>
                                        <input id="form_name" type="text" name="name" class="form-control" placeholder="Inserisci il tuo nome" required="required" data-error="Nome richiesto.">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="form_lastname">Cognome *</label>
                                        <input id="form_lastname" type="text" name="surname" class="form-control" placeholder="Inserisci il tuo cognome" required="required" data-error="Cognome richiesto">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="form_email">Email *</label>
                                        <input id="form_email" type="email" name="email" class="form-control" placeholder="Inserisci la tua email" required="required" data-error="Valid email is required.">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="form_need">Di cosa hai bisogno? *</label>
                                        <select id="form_need" name="need" class="form-control" required="required" data-error="Di cosa hai bisogno.">
                                            <option value="">Informazioni generali</option>
                                            <option value="Assistenza ordine">Assistenza ordine</option>
                                            <option value="Informazioni prodotto">Informazioni prodotto</option>
                                            <option value="Altro">Altro</option>
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="form_message">Messaggio *</label>
                                        <textarea id="form_message" name="message" class="form-control" placeholder="Inserisci il messaggio che vuoi inviarci" rows="4" required="required" data-error="Please, leave us a message."></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col text-center">
                                    <input type="submit" class="btn btn-dark btn-send align-left" value="Invia messaggio">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <p class="text-white">
                                        <strong>*</strong> Questi campi sono obbligatori.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>