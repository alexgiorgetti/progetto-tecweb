<div class="card text-center" style="max-width:250px; padding:20px">
    <div>
        <img class="" src="<?php echo $value['Immagine'] ?>" alt="Sample" style="height:250px;"/>

        <div class="text-center pt-4">

            <h5 class="mb-0"><?php echo $value['Titolo'] ?></h5>
            <h6 class="mb-3">€ <?php echo $value['Prezzo'] ?></h6>
            <a href="visualizza.php?id=<?php echo $value['libroID'] ?>" class="btn btn-outline-primary btn-md mr-1 mb-2 waves-effect waves-light">Dettagli</a>

        </div>

    </div>
</div>