<div class= "container rounded border border-warning mb-8 my-4 p-4">
    <header class="section-header text-center">
        <h5 style="padding:1%">
            <strong>Carrello</strong>
        </h5> 
    </header>
    <?php if(isset($_SESSION['cart'])): 
        $total_price = 0.00;
    ?>
    <form>
            <div class="row candy">
                <div class="col-8"></div>
                <div class="col-sm-2"><h4 class="text-center"> Quantità  </h4></div>
                <div class="col-sm-2"><h4 class="text-center"> Prezzo </h4></div>
            </div>
        <?php foreach($_SESSION['cart'] as $key => $value): ?>
            <?php
                $book = $db->getBookbyId($key)[0]; 
                $total_price += (int)$book["Prezzo"] * $value;	
            ?>
            <hr class="mt-2 mb-3"/>
            <div class="row">
                <div class="col-3 candy">
                    <img class="img w-25 pl-3" src="<?php echo $book["Immagine"]; ?>" alt="" />
                </div>
                <div class="col-4 col-md-5 center-text"> 
                    <h5><?php echo $book["Titolo"]; ?></h5>
                </div>
                <div class="col-5 col-md-3 center-text">
                    <label for="<?php echo $key; ?>">Numero copie:</label>
                    <input style="width:100px" class="form-control quantchange" type="number" name="quantity" id="<?php echo $key; ?>" min="1" value="<?php echo $value; ?>"/>
                    <div class="align-bottom"><a href="checkout.php?action=1&id=<?php echo $book["libroID"]; ?>" title="Remove from Cart">Rimuovi dal carrello</a></div>
                </div>
                <div class="col-3 col-md-1 center-text">
                    <strong><?php echo $book["Prezzo"]."€"; ?></strong>
                </div>
            </div>
        <?php endforeach; ?>
    </form>
    <div class="row" style="margin-top:5px;">
        <div class="col-md-9"><a href="checkout.php?action=2;"> <strong> Rimuovi tutti i libri </strong></a> </div>
        <div class="col-md-3" id="total_price"> <strong> Prezzo totale:  <?php echo number_format($total_price,2) . " €"; ?></strong></div>
    </div>
    <?php else: ?>
        <header class="section-header text-center">
            <h6 style="padding:1%">
                <strong>Il tuo carrello è vuoto.</strong>
            </h6>
        </header>
    <?php endif; ?>
</div>

<div class= "container rounded border border-warning mb-8 my-4 p-4">
    <header class="section-header text-center">
        <h5 class="text-center"> Dati di spedizione </h5>
    </header>
    <?php require_once 'checkoutInfo.php'; ?>
</div>