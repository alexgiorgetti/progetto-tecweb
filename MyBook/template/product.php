<div class="container rounded border border-warning mb-2 my-4 p-4">
    <div class="row">
        <div class="col-md-5 mb-4 mb-md-0">
            <div class="view zoom z-depth-2 rounded">
                <img class="img w-100" src=<?php echo $all_info['Immagine']?> alt="Sample">
                <a href="#!">
                <div class="mask waves-effect waves-light"></div>
                </a>
            </div>
        </div>

        <div class="col-md-7">
            <h5><?php echo $all_info['Titolo']?></h5>
            <p class="mb-2 text-muted text-uppercase small"><?php echo $all_info['Genere']?></p>
            <p><span class="mr-1"><strong>€ <?php echo $all_info['Prezzo']?></strong></span></p>
            <p class="pt-1"><?php echo $all_info['Trama']?></p>

            <div class="table-responsive">
                <table class="table table-sm table-borderless mb-0">
                <tbody>
                    <tr>
                    <th class="pl-0 w-25" scope="row"><strong>Venditore</strong></th>
                    <td><?php echo $all_info['Nome']?> <?php echo $all_info['Cognome']?></td>
                    </tr>
                    <tr>
                    <th class="pl-0 w-25" scope="row"><strong>Casa Editrice</strong></th>
                    <td><?php echo $all_info['CE_Nome']?></td>
                    </tr>
                    <tr>
                    <th class="pl-0 w-25" scope="row"><strong>Autore</strong></th>
                    <td><?php echo $all_info['A_Nome']?> <?php echo $all_info['A_Cognome']?></td>
                    </tr>
                </tbody>
                </table>
            </div>
            <hr>

            <form action="addProduct.php" method="GET">
                <div class="table-responsive mb-2">
                    <table class="table table-sm table-borderless">
                    <tbody>
                        <tr>
                            <td class="pl-0">Quantità</td>
                        </tr>
                        <tr>
                            <td class="pl-0">
                                <div class="input-group">
                                    <button type="button" onclick="this.parentNode.querySelector('input[type=number]').stepDown()" class="btn btn-outline-danger" data-type="minus" style="margin-right:10px">
                                        -
                                    </button>

                                    <input type="text" name="id" class="form-control" value="<?php echo $all_info['libroID']?>" style='display: none;'>

                                    <input type="number" name="quantity" class="form-control" value="1" min="1" max="10">
                                    <button type="button" onclick="this.parentNode.querySelector('input[type=number]').stepUp()" class="btn btn-outline-success" data-type="plus" style="margin-left:10px">
                                        +
                                    </button>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                    </table>
                </div>

                <?php

                    if(isset($_SESSION['cart'])){
                        foreach($_SESSION['cart'] as $key => $value){
                            $sellerBookC = $db->getBookbyId($key)[0]['venditoreID'];
                            $sellerBookP = $db->getBookbyId($_GET['id'])[0]['venditoreID'];

                            if($sellerBookC != $sellerBookP){
                                echo '<button type="submit" class="btn btn-outline-danger btn-md mr-1 mb-2 waves-effect waves-light" disabled>Disabilitato</button>';
                            } else
                                echo '<button type="submit" class="btn btn-outline-primary btn-md mr-1 mb-2 waves-effect waves-light">Aggiungi al carrello</button>';

                            break;
                        }
                    } else 
                        if($all_info['Disponibile'] == 0){
                            if(isset($_SESSION['Login']) && $_SESSION['Login'] == True)
                                if ($_SESSION['utenteID'] != $all_info['utenteID'] && $_SESSION['Ruolo'] != 2){
                                    echo '<button type="submit" class="btn btn-outline-primary btn-md mr-1 mb-2 waves-effect waves-light">Aggiungi al carrello</button>';
                                }
                                else {
                                    echo '<button type="submit" class="btn btn-outline-danger btn-md mr-1 mb-2 waves-effect waves-light" disabled>Non disponibile</button>';
                                }
                            else { 
                                echo '<button type="submit" class="btn btn-outline-primary btn-md mr-1 mb-2 waves-effect waves-light">Aggiungi al carrello</button>';
                            }
                        } else {
                            echo '<button type="submit" class="btn btn-outline-danger btn-md mr-1 mb-2 waves-effect waves-light" disabled>Non disponibile</button>';
                        }
                ?>
            </form>
        </div>
    </div>

    </div>

    <hr>

    <div class="box" id="corr">
    <div class="container mb-2 my-5 p-5">

        <div class="row justify-content-center">
            <h4 style="padding:1%">
                <strong>Prodotti Correlati</strong>
            </h4>
        </div>
        
        <div class="row justify-content-center">
            <?php
                $libri = $db->allBooks();
                $stop = False;
                $maxBook = 0;

                foreach($libri as $key => $value){
                    
                    if($maxBook < 5 && $value['libroID'] != $all_info['libroID']){
                        include 'productRelated.php';
                    }

                }
            ?>
        </div>

    </div>
</div>