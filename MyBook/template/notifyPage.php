<div class="container rounded border border-warning mb-2 my-4 p-4">
    <div class="row justify-content-center">
        <h4 style="padding:1%">
            <strong>Notifiche</strong>
        </h4>
    </div>

    <div id="accordion">
        <?php
            $_SESSION['Notifiche'] = $db->getNotify($_SESSION['utenteID']);
            foreach($_SESSION['Notifiche'] as $key => $value){

                if($value['utenteID_S'] == -1){
                    $nome = 'Form';
                    $cognome = 'Contatti';
                } else if ($value['utenteID_S'] == -2){
                    $nome = 'Admin';
                    $cognome = 'Ordini';
                } else {
                    $usr2 = $db->sellerOfBook($value['utenteID_S'])[0];
                    $nome = $usr2['Nome'];
                    $cognome = $usr2['Cognome'];
                }

                include 'notifyInfo.php';

            }
            
        ?>
    </div>
</div>