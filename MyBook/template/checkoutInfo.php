<form class style="padding:2%" id="contact-form" method="post" action="processa-ordine.php">

    <div class="controls">

        <div class="row">
            <div class="col-md-6">
                <div class="form-group text-center">
                    <label  class="form-control"><?php echo $_SESSION['Nome'] ?></label>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group text-center">
                    <label class="form-control" ><?php echo $_SESSION['Cognome'] ?></label>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group text-center">
                    <label  class="form-control" ><?php echo $_SESSION['Username'] ?></label>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group text-center">
                    <label class="form-control" ><?php echo $_SESSION['Telefono'] ?></label>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group text-center">
                    <label  class="form-control" ><?php echo $_SESSION['DataNascita'] ?></label>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group text-center">
                    <label class="form-control" ><?php echo $_SESSION['IndirizzoResidenza'] ?></label>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group text-center">
                    <label class="form-control" ><?php echo $_SESSION['CittaResidenza'] ?></label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label for="form_need"><strong>Pagamento:</strong></label>
                <select id="form_need" name="need" class="form-control" required="required" data-error="Please specify your method.">
                    <option value="">Scegliere un metodo</option>
                    <option value="paypal">PayPal</option>
                    <option value="card">Contrassegno</option>
                </select>
            </div>
        </div>
 
        <div class="row" style="margin-top:10px;">
            <div class="col text-center">
                <input name="confirm" type="submit" class="btn btn-dark btn-send align-left" value="Conferma ordine">
            </div>
        </div>  
    </div>
</form>



