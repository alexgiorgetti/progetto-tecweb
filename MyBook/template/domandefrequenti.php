<div class="accordion container rounded border border-warning mb-8 my-4 p-4 ">
    <div class="page-header text-center">
        <h5>FAQ - Domande Frequenti  </h5>
    </div>
        <div class="row float-right">
        <button class=" btn text-success close-all " style="margin-bottom:5px">Riduci tutto</button>
        </div>
        <br/>
        <hr class="mt-2 mb-3"/>
        <div class="border rounded " style="margin:5px;">
            <button class="btn-block btn  btn-primary">Spedizioni</button>
                <div>
                    <button class="btn-block btn  btn-primary">Tracciare il nostro pacco</button>
                        <div>
                            Andando nella schermata utente è possibile vedere a che punto sono i propri ordini.
                        </div>
                        <button class="btn-block btn  btn-primary">Reso</button>
                        <div>
                            Per rendere il pacco aspettare la consegna e rivolgersi direttamente al corriere.
                        </div>
                        <button class="btn-block btn  btn-primary">Tempi di spedizione</button>
                        <div>
                            Le nostre spedizioni sono veloci ed affidabili, normalmente l'ordine arriva in una settimana lavorativa.
                        </div>
                </div>
        </div>
        <div class="border rounded " style="margin:5px;">
            <button class="btn-block btn  btn-primary">Prodotti</button>
                <div>
                    Tutti i prodotti li trovi nella homepage e puoi effettuare ricerca sui generi disponibili, sull'autore e ricercando un titolo.
                </div>
        </div>
        <div class="border rounded " style="margin:5px;">
            <button class="btn-block btn  btn-primary">Chi siamo</button>
                <div>
                    Progetto per l'Università Ignegneria Scienza Informatiche Cesena.
                </div>
        </div>
</div>