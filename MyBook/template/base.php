<!DOCTYPE html>
<html lang="it">
    <head>
    <?php require 'head.php' ?>
    <?php
    if(isset($templateParams["css"])):
        foreach($templateParams["css"] as $script):
    ?>
        <link href="<?php echo $script; ?>" rel="stylesheet">
    <?php
        endforeach;
    endif;
    ?>
    <title><?php echo $templateParams["titolo"]; ?></title>
    <?php
    if(isset($templateParams["js"])):
        foreach($templateParams["js"] as $script):
    ?>
        <script src="<?php echo $script; ?>"></script>
    <?php
        endforeach;
    endif;
    ?>
</head>
<body>
    <?php require 'navbar.php' ?>
    <main>
    <?php
    if(isset($templateParams["nome"])){
        require($templateParams["nome"]);
    }
    ?>
    </main>
    <?php require 'footer.html' ?>
</body>
</html>