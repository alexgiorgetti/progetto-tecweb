<div class="card">
    <div class="card-header" id="heading<?php echo $value['notificaID'] ?>">
        <h5 class="mb-0">
            <button class="btn btn-link" data-toggle="collapse" data-target="#collapse<?php echo $value['notificaID'] ?>" aria-expanded="true" aria-controls="collapse<?php echo $value['notificaID'] ?>"> <?php echo $value['Oggetto'] ?> </button>
        </h5>
    </div>

    <div id="collapse<?php echo $value['notificaID'] ?>" class="collapse" aria-labelledby="heading<?php echo $value['notificaID'] ?>" data-parent="#accordion">
        <div class="card-body"> 
            Messaggio inviato da: <strong><?php echo $nome." ".$cognome ?></strong> 
            <p><?php echo $value['Messaggio'] ?></p> 
            <a href="removeNotify.php?notifica=<?php echo $value['notificaID'] ?>">
                <button class="btn btn-outline-primary">
                    Conferma lettura
                </button>
            </a> 
        </div>
    </div>
</div>