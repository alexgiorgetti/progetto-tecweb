<div class="card text-center" style="max-width:250px">
    <img class="card-img-top" src="<?php echo $value['Immagine'] ?>" alt="<?php echo $value['Titolo'] ?>" style="height:250px;" >
    <div class="card-body">
        <h5 class="card-title"><?php echo $value['Titolo'] ?></h5>
        <h6 class="card-subtitle mb-2 text-muted text-center">Autore: <?php echo $author[0]['Nome']." ".$author[0]['Cognome'] ?></h6>
        <h6 class="card-subtitle mb-2 text-muted text-center">Venditore: <?php echo $seller[0]['Nome']." ".$seller[0]['Cognome'] ?></h6>
        <h6 class="card-subtitle mb-2 text-muted text-center">Casa editrice: <?php echo $editor[0]['Nome'] ?></h6>
        <p class="card-text"> 
            <div class="price text-success text-bottom">
                <h5> <?php echo $value['Prezzo'] ?> € </h5>
            </div>
            <a href="visualizza.php?id=<?php echo $value['libroID'] ?>" class="btn btn-danger btn-primary">
                Acquista libro
            </a>
        </p>
    </div>
</div>