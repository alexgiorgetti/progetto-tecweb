<?php
    require("bootstrap.php");
    error_reporting(E_ALL);

    //Base Template
    $templateParams["titolo"] = "Admin";
    $templateParams["nome"] = "admin.php";
    $templateParams["css"] = array("class/css/user_form.css");

    if (!isset($_SESSION['Login']) || !$_SESSION['Login']){
        header("location:login.php");   
    }

    require_once 'template/base.php';
?>