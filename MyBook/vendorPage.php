<?php
    require("bootstrap.php");
    error_reporting(E_ALL);

    if (!isset($_SESSION['Login']) || !$_SESSION['Login']){
        header("location:login.php");   
    }

    if(isset($_GET['view'])){
        $showOrder = $_GET['view'];
        $book = json_decode($db->getOrder($showOrder)[0]['LibroOrdinato']);
    } else {
        $showOrder = -1;
        $book = array();
    }
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="stylesheet"  href="class/css/user_form.css ">
        <?php include 'head.php' ?>
        <?php include 'navbar.php'?>
        <title>Venditore</title>

        <script>
            $(document).ready(function() {
                var showOrder = <?php echo $showOrder; ?>;
                if(showOrder != -1){
                    <?php
                        echo "$('#modalText').append('Ordine ".$showOrder.":<br />');";
                        
                        foreach($book as $key => $value) {
                            echo "$('#modalText').append('Libro: ".$db->getBookbyId($key)[0]['Titolo']." - Quantità: ".$value."<br />');";
                        }
                        
                    ?>
                    $('#sellerModal').modal('toggle');
                    $('#sellerModal').modal('show');
                }
            });
        </script>
    </head>
    <body>

        <!-- Modal -->
        <div class="modal fade" id="sellerModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="sellerModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title title-imp" id="sellerModalLabel">Attenzione</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body text-write">
                        <p id="modalText"></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-warning" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="box">  
            <div class= "container rounded border border-warning mb-2 my-4 p-4 bg-faded ">
                <div class="row justify-content-center ">
                    <h1  style="padding:1%">
                        <strong>Pannello Venditore</strong>
                    </h1>
                </div>
                <div class="box">
                    <div class= "container rounded border border-warning mb-2 my-4 p-4 bg-faded " style="overflow-x:auto;"> 
                        <div class="row justify-content-center">
                                <h2  style="padding:1%">
                                    <strong>Lista libri in vendita</strong>
                                </h2>
                        </div> 
                                        
                        <table class="table">
                            <thead class="thead-dark text-center">
                                <tr>
                                <th scope="col">ID Libro</th>
                                <th scope="col">Titolo</th>
                                <th scope="col">Prezzo</th>
                                <th scope="col">Genere</th>
                                <th scope="col">Autore</th>
                                <th scope="col">Casa editrice</th>
                                <th scope="col">Disponibile</th>
                                <th scope="col">Azioni</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php      
                                $tmp = $db->libriVenditore($_SESSION['utenteID']);
                                
                                foreach($tmp as $libri){

                                    $libroId = $libri['libroID'];
                                    $titolo = $libri['Titolo'];
                                    $prezzo = $libri['Prezzo'];
                                    $genere = $libri['Genere'];
                                    $disponibile = ($libri['Disponibile']);
                                    $autore=$libri['autoreID'];
                                    $casaEditrice=$libri['casaEditriceID'];

                                    $author = $db->authorOfBook($autore);
                                    
                                    foreach($author as $autore){
                                        $nomeAutore = $autore['Nome'];
                                        $cognomeAutore = $autore['Cognome'];   
                                    }    

                                    $tmp = $db->editorOfBook($casaEditrice);

                                    foreach($tmp as $casaEditrice){
                                        $nomeCasaEditrice = $casaEditrice['Nome'];
                                    }  

                                    if ($disponibile==0) $disponibilita='SI';
                                    else $disponibilita='NO';

                                    echo "<tr>";
                                
                                    echo "<td>{$libroId}</td>";
                                    echo "<td>{$titolo}</td>";
                                    echo "<td>{$prezzo}€</td>";
                                    echo "<td>{$genere}</td>";
                                    echo "<td>{$nomeAutore} {$cognomeAutore}</td>";
                                    echo "<td>{$nomeCasaEditrice}</td>";
                                    echo "<td>{$disponibilita}</td>";
                                    echo "<td><a class='btn btn-success' href='changeAvailable.php?editAvailability={$libroId}&editAvailable={$disponibilita}'><span class='fa fa-edit'></span></a> <a class='btn btn-danger' href='vendorPage.php?deleteBook={$libroId}'><span class='fa fa-trash'></span></a> </td>";

                                    echo "</tr>";
        
                                }  
                            ?>
                            <?php

                            if (isset($_GET['deleteBook'])) {
                                $libroId = $_GET['deleteBook'];

                                $query = "DELETE FROM Libro WHERE libroID = ?";

                                $db->newQuery($query);
                                $db->bindInQuery("s",$libroId);
                                $db->executeQuery();

                                header("location:vendorPage.php");
                            }
                            
                            ?>
                            </tbody>
                        </table>      
                    </div>

                    <div class="box">
                        <div class= "container rounded border border-warning mb-2 my-4 p-4 bg-faded " style="overflow-x:auto;"> 
                            <div class="row justify-content-center">
                                    <h2  style="padding:1%">
                                        <strong>Lista ordini</strong>
                                    </h2>
                            </div> 
                                            
                            <table class="table">
                                <thead class="thead-dark text-center">
                                    <tr>
                                    <th scope="col">ordineID</th>
                                    <th scope="col">Data ordine</th>
                                    <th scope="col">Stato</th>
                                    <th scope="col">Azioni</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    $ord = $db->ordineVenditore($_SESSION['utenteID']);

                                    foreach ($ord as $ordine) {
                                        
                                        $ordineId = $ordine['ordineID'];
                                        $libro = $ordine['LibroOrdinato'];
                                        $dataOrdine = $ordine['DataOrdine'];
                                        $stato = $ordine['Stato'];   

                                        echo "<tr>";

                                        echo "<td><a href='vendorPage.php?view={$ordineId}'>{$ordineId}</a></td>";
                                        echo "<td>{$dataOrdine}</td>"; 
                                        echo "<td>{$stato}</td>"; 

                                        if ($stato != 'Spedito' && $stato != 'Annullato'){
                                            echo "<td><a class='btn btn-primary' href='changeOrder.php?idOrder={$ordineId}&Stato=Spedito'><span class='fa fa-plane'></span></a> <a class='btn btn-danger' href='changeOrder.php?idOrder={$ordineId}&Stato=Annullato'><span class='fa fa-times'></span></a></td>";
                                        }
                                        else{ 
                                            echo "<td><a class='btn btn-danger' href='changeOrder.php?idOrder={$ordineId}&Stato=Eliminato'><span class='fa fa-trash'></span></a></td>";
                                        }

                                        echo "</tr>";
                                    }
                                ?>
                                <?php
                                if (isset($_GET['deleteBook'])) {
                                    $libroId = $_GET['deleteBook'];

                                    $query = "DELETE FROM Libro WHERE libroID = ?";

                                    $db->newQuery($query);
                                    $db->bindInQuery("s",$libroId);
                                    $db->executeQuery();

                                    header("location:vendorPage.php");
                                }
                                
                                ?>
                                </tbody>
                            </table>      
                        </div> 
                    </div>

                    <div class="box">
                        <div class= "container rounded border border-warning mb-2 my-4 p-4">
                            <div class="row justify-content-center">
                                <h2  style="padding:1%">
                                    <strong>Nuovo Libro</strong>
                                </h2>
                            </div>
                            <form id="user-form" method="post" action="vendorPage.php">
                                <?php
                                    if (isset($_POST['add'])){
                                    
                                        $addTitle=$_POST['title'];
                                        $addPrice=$_POST['price'];
                                        $addType=$_POST['type'];
                                        $addPlot=$_POST['plot'];
                                        $addImage='img/'.$_POST['image'].'.jpg';               
                                        $addVendor=$_SESSION['utenteID'];
                                        $addAvailable=0;
                                        $addAuthor=$db->authorOfBook($_POST['author'])[0]['autoreID'];
                                        $addHouse=$db->editorOfBook($_POST['house'])[0]['casaEditriceID'];


                                        $update="INSERT INTO Libro(Titolo,Prezzo,Genere,Trama,Immagine,CasaEditriceID,AutoreID,VenditoreID,Disponibile)
                                        VALUES('{$addTitle}','{$addPrice}','{$addType}','{$addPlot}','{$addImage}','{$addHouse}','{$addAuthor}','{$addVendor}','{$addAvailable}');";

                                        $db->newQuery($update);   
                                        $db->executeQuery();

                                        header("location:vendorPage.php");
                                    }
                                ?>
                                <div class="controls">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group text-center">
                                                <label for="titolo" class="text-center">Titolo</label>
                                                <input id="titolo" type="text" type="text" name="title" class="form-control" required="required" data-error="Titolo richiesto." placeholder="Inserisci il titolo">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group text-center">
                                                <label for="prezzo">Prezzo</label>
                                                <input id="prezzo" type="text" type="text" name="price" class="form-control"  required="required" data-error="Prezzo richiesto" placeholder="Inserisci il prezzo">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class = "row">
                                        <div class="col-md-6">
                                            <div class="form-group text-center">
                                                <label for="genere">Genere</label>
                                                <input id="genere" type="text"  type="text" name="type" class="form-control"  required="required" data-error="Genere richiesto." placeholder="Inserisci il genere">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group text-center">
                                                <label for="immagine">Immagine</label>
                                                <input id="immagine" type="text"  name="image" class="form-control"  required="required" data-error="Inserisci un Immagine" placeholder="Inserisci il nome del file con estensione">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group text-center">
                                            <label for="autore">Autore</label>
                                                <select class="form-control" id="autore" name="author" >
                                                <?php 
                                                    $autori = $db->checkAuthors();
                                                    foreach ($autori as $key => $value) {
                                                        echo "<option value='".$value['autoreID']."'>".$value['Nome']." ".$value['Cognome']."</option>";
                                                    }
                                                ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group text-center">
                                                <label for="casaEditrice">Casa editrice</label>
                                                <select class="form-control" id="casaEditrice" name="house">
                                                    <?php 
                                                        $casa = $db->checkEditors();
                                                        foreach ($casa as $key => $value) {
                                                            echo "<option value='".$value['casaEditriceID']."'>".$value['Nome']."</option>";
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group text-center">
                                                <label for="trama">Trama</label>
                                                <textarea id="trama" name="plot" class="form-control" rows="4" required="required" data-error="Inserisci una trama." ></textarea>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="col text-center">
                                            <input type="submit" class="btn btn-success" value="Aggiungi libro" name="add">
                                        </div>
                                    </div>   
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include 'footer.html' ?>              
    </body>
</html>