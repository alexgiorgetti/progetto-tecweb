<?php
    require_once 'bootstrap.php';
    //Base Template
    $templateParams["titolo"] = "Checkout";
    $templateParams["nome"] = "cart.php";
    $templateParams["js"] = array("js/cart.js");
    $templateParams["css"] = array("class/css/checkout.css");

    if (!isset($_SESSION['Login']) || !$_SESSION['Login']){
        header("location:login.php");   
    }

    $var = true;

    if(!empty($_GET["action"])){    
        if($_GET["action"] == 1 ){
            if(!empty($_SESSION['cart'])) {
                foreach($_SESSION["cart"] as $k => $v) {
                        if($_GET["id"] == $k)
                            unset($_SESSION["cart"][$k]);				
                        if(empty($_SESSION["cart"]))
                            unset($_SESSION["cart"]);
                }
            }   
        }

        if($_GET["action"] == 2){
            unset($_SESSION["cart"]);
        }
    }
    if(!empty($_POST["action"])){
        if($_POST["action"] == 3){
            $var=false;
            if($_POST["quantity"] <= 0){
                unset($_SESSION['cart'][$_POST["id"]]);
                if(empty($_SESSION["cart"]))
                    unset($_SESSION["cart"]);
            }
            else{
                $total_price = 0;
                foreach ($_SESSION['cart'] as $k => $v) {
                    if($_POST["id"] == $k) {
                        $_SESSION['cart'][$k] = $_POST["quantity"];
                    }
                    $book = $db->getBookbyId($k)[0];
                    $total_price += $book["Prezzo"] * (int)$_SESSION['cart'][$k];	
                }
                if($total_price!=0 && is_numeric($total_price)) {
                    echo '<strong> Prezzo totale:  ' .number_format($total_price,2).' € </strong>';
                }
            }
        }
    }
    if($var == true)
        require_once 'template/base.php';

?>

