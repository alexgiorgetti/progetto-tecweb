<?php
    /* Qui settiamo le info senza doverle riscriverle ogni volta
    e se dovesse cambiare il DB le abbiamo tutte pronte*/

    $host="localhost";
    $database="mybook";
    $user="root";
    $pass="";
    $port=3307;
   
    define("DB_HOST", $host);
    define("DB_USER", $user);
    define("DB_PASS", $pass);
    define("DB_NAME", $database);
    define("DB_PORT", $port);
?>