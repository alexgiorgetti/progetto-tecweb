<?php

require_once ('config.php');

ob_start();

class Database{

    private $host      = DB_HOST;
    private $user      = DB_USER;
    private $pass      = DB_PASS;
    private $dbname    = DB_NAME;
    private $port      = DB_PORT;

    private $db;
    private $stmt;

    public function __construct(){
        $this->db = new mysqli($this->host, $this->user, $this->pass, $this->dbname, $this->port);
        
        if($this->db->connect_error){
            die("Connessione al db fallita.");
        }
    }

    public function close(){
        $this->db=null;
        return true;
    }

    public function newQuery($query){
        $this->stmt = $this->db->prepare($query);
    }

    public function bindInQuery($type, $value){
        $this->stmt->bind_param($type, $value);
    }

    public function executeQuery(){
    	try{
        	return $this->stmt->execute();        
        } catch (Exception $e){
        	echo $e->getmessage();
        }
    }

    public function resultQuery(){
        $this->executeQuery();
        $result = $this->stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function singleResultQuery(){
        $this->executeQuery();
        $result = $this->stmt->get_result();
        return $result->fetch(MYSQLI_ASSOC);
    }

    public function newUser($name, $surname, $username, $password, $phone, $bdate, $address, $city, $role){

        $query= "   INSERT INTO Utente (Nome, Cognome, Username, Password,
                        Telefono, DataNascita, IndirizzoResidenza, CittaResidenza, Ruolo)
                        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);";
        
        $this->newQuery($query);
        $this->stmt->bind_param("ssssssssi", $name, $surname, $username, $password, $phone, $bdate, $address, $city, $role);
        $this->executeQuery();
    }

    public function newNotify($userS, $userD, $oggetto, $messaggio){

        $query= "   INSERT INTO Notifica (Oggetto, Messaggio, utenteID_S, utenteID_D)
                        VALUES (?, ?, ?, ?);";
        
        $this->newQuery($query);
        $this->stmt->bind_param("ssii", $oggetto, $messaggio, $userS, $userD);
        $this->executeQuery();
    }

    public function checkAuthors(){
        $query= "SELECT * FROM Autore";
        
        $this->newQuery($query);
        return $this->resultQuery();
    }

    public function checkEditors(){
        $query= "SELECT * FROM CasaEditrice";
        
        $this->newQuery($query);
        return $this->resultQuery();
    }

    public function allUser(){
        $query= "SELECT * FROM Utente";

        $this->newQuery($query);
        return $this->resultQuery();
    }

    public function allAdmin(){
        $query= "SELECT * 
                    FROM Utente
                    WHERE Utente.Ruolo = 2";

        $this->newQuery($query);
        return $this->resultQuery();
    }
    
    public function allBooks(){
        $query= "SELECT * FROM Libro";
        
        $this->newQuery($query);
        return $this->resultQuery();
    }

    public function authorOfBook($author){
        $query= "SELECT * 
                    FROM Autore
                    WHERE Autore.autoreID = ?";
        
        $this->newQuery($query);
        $this->stmt->bind_param("s", $author);
        return $this->resultQuery();
    }

    public function sellerOfBook($seller){
        $query= "SELECT * 
                    FROM Utente
                    WHERE Utente.utenteID = ?";
        
        $this->newQuery($query);
        $this->stmt->bind_param("s", $seller);
        return $this->resultQuery();
    }

    public function editorOfBook($editor){
        $query= "SELECT * 
                    FROM CasaEditrice
                    WHERE CasaEditrice.casaEditriceID = ?";
        
        $this->newQuery($query);
        $this->stmt->bind_param("s", $editor);
        return $this->resultQuery();
    }

    public function checkGen(){
        $query= "SELECT * 
                    FROM Libro";
        
        $this->newQuery($query);
        return $this->resultQuery();
    }

    public function bookByGen($kind){
        $param = '%'.$kind.'%';
        $query= "SELECT * 
                    FROM Libro
                    WHERE Libro.Genere LIKE ?";
        
        $this->newQuery($query);
        $this->stmt->bind_param("s", $param);
        return $this->resultQuery();
    }

    public function searchBook($title, $author){

        if($title != null && $author == 'Autore'){
            $param = '%'.$title.'%';
            $query= "SELECT * 
                    FROM Libro
                    WHERE Libro.Titolo LIKE ?";
        
            $this->newQuery($query);
            $this->stmt->bind_param("s", $param);
            return $this->resultQuery();
        }

        if($title == null && $author != 'Autore'){
            $query= "SELECT * 
                    FROM Libro
                    WHERE Libro.autoreID = ?";
        
            $this->newQuery($query);
            $this->stmt->bind_param("s", $author);
            return $this->resultQuery();
        }

        $query= "SELECT * 
                    FROM Libro
                    WHERE Libro.Titolo LIKE '%?%'
                    AND Libro.autoreID = ?";
        
        $this->newQuery($query);
        $this->stmt->bind_param("ss", $title, $author);
        return $this->resultQuery();

    }

    public function libriVenditore($utente){
        $libriVendita="SELECT *
                        FROM Libro
                        WHERE Libro.venditoreID = ? ";
        
        $this->newQuery($libriVendita);
        $this->bindInQuery("s",$utente);
        return $this->resultQuery();
    }

    public function ordineUtente($utente){
        $ordiniUtente="SELECT *
                        FROM Ordine
                        WHERE Ordine.utenteID= ? ";
        
        $this->newQuery($ordiniUtente);
        $this->bindInQuery("s",$utente);
        return $this->resultQuery();
    }

    public function allInfoFromBook($id){
        $query= "SELECT Libro.*, Autore.Nome AS A_Nome, Autore.Cognome AS A_Cognome, CasaEditrice.Nome AS CE_Nome, Utente.*
                    FROM Libro
                    INNER JOIN Autore ON Libro.autoreID = Autore.autoreID
                    INNER JOIN CasaEditrice ON Libro.casaEditriceID = CasaEditrice.casaEditriceID
                    INNER JOIN Utente ON Libro.venditoreID = Utente.utenteID
                    WHERE Libro.libroID = ? LIMIT 1";
        
        $this->newQuery($query);
        $this->stmt->bind_param("i", $id);
        return $this->resultQuery();
    }

    public function getNotify($user){
        $query= "SELECT * 
                    FROM Notifica
                    WHERE Notifica.utenteID_D = ?";
        
        $this->newQuery($query);
        $this->stmt->bind_param("s", $user);
        return $this->resultQuery();
    }

    public function removeNotify($id){
        $query= "DELETE FROM Notifica
                    WHERE Notifica.notificaID = ?";
        
        $this->newQuery($query);
        $this->stmt->bind_param("s", $id);
        $this->executeQuery();
    }

    public function getBookbyId($idBook){
        $query= " SELECT * 
                    FROM Libro 
                    WHERE libroID = ?";
        
        $this->newQuery($query);
        $this->stmt->bind_param("i", $idBook);
        return $this->resultQuery();
 
    }

    public function getOrder($ordineId){
        $query = "SELECT * 
                    FROM Ordine 
                    WHERE Ordine.ordineID = ?";
        
        $this->newQuery($query);
        $this->stmt->bind_param("s",$ordineId);
        return $this->resultQuery(); 
 
    }
 
    public function newOrder($utenteID, $dataOrdine, $stato, $libriOrdinati, $venditoreID){
        $query= "   INSERT INTO Ordine (LibroOrdinato, DataOrdine, Stato, utenteID, venditoreID)
                        VALUES (?, ?, ?, ?, ?);";
        
        $this->newQuery($query);
        $this->stmt->bind_param("sssss", $libriOrdinati, $dataOrdine, $stato, $utenteID, $venditoreID);
        $this->executeQuery();
    }

    public function ordineVenditore($utente) {
        $ordiniVenditore = "SELECT *
                            FROM Ordine
                            WHERE Ordine.venditoreID = ?";
        
        $this->newQuery($ordiniVenditore);
        $this->stmt->bind_param("s", $utente);
        return $this->resultQuery();
    }

}

?>