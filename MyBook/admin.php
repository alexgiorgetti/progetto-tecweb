<div class="box">
    <div class= "container rounded border border-warning mb-2 my-4 p-4 bg-faded ">  
        <div class="row justify-content-center ">
            <h1  style="padding:1%">
                <strong>Pannello Amministrazione</strong>
            </h1>
        </div> 
        <div class="box">             
            <div class= "container rounded border border-warning mb-2 my-4 p-4 bg-faded " style="overflow-x:auto;"> 
                <div class="row justify-content-center">
                        <h2  style="padding:1%">
                            <strong class="text-center">Lista Utenti Registrati</strong>
                        </h2>
                </div> 
                                
                <table class="table">
                    <thead class="thead-dark text-center">
                        <tr>
                        <th scope="col">ID Utente</th>
                        <th scope="col">Nome</th>
                        <th scope="col">Cognome</th>
                        <th scope="col">Ruolo</th>
                        <th scope="col">Modifica</th>
                        <th scope="col">Elimina</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php      
                        $tmp = $db->allUser();
                        
                        foreach($tmp as $key => $value){

                            $utenteId = ($value['utenteID']);
                            $nome = ($value['Nome']);
                            $cognome =($value['Cognome']);
                            $ruolo = ($value['Ruolo']);

                            echo "<tr>";

                            echo "<td>{$utenteId}</td>";
                            echo "<td>{$nome}</td>";
                            echo "<td>{$cognome}</td>";
                            echo "<td>{$ruolo}</td>";
                            echo "<td><a class='btn btn-success' href='adminPage.php?editUser={$utenteId}'><strong class='fa fa-wrench'></strong></a></td>";
                            echo "<td><a class='btn btn-danger' href='adminPage.php?deleteUser={$utenteId}'><strong class='fa fa-trash'></strong></a></td>";

                            echo "</tr>";

                        }  
                    ?>
                    <?php
                    if (isset($_GET['deleteUser'])) {
                        $utenteId = $_GET['deleteUser'];

                        $query = "DELETE FROM Utente WHERE utenteID = ?";

                        $db->newQuery($query);
                        $db->bindInQuery("s",$utenteId);
                        $db->executeQuery();

                        header("location:adminPage.php");
                    
                    }
                    ?>
                    </tbody>
                </table>      
            </div>
        <div class="box">
            <div class= "container rounded border border-warning mb-2 my-4 p-4"> 
                <form action="changeRole.php" method="post">
                <?php
                    if (isset($_GET['editUser'])) {
                        $utenteId = $_GET['editUser'];

                        $query = "SELECT * FROM Utente WHERE utenteID = ?";

                        $db->newQuery($query);
                        $db->bindInQuery("s",$utenteId);
                        $row=$db->resultQuery()[0];
                            
                        $utenteId=$row['utenteID'];
                        $nome=$row['Nome'];
                        $cognome=$row['Cognome'];
                        $ruolo=$row['Ruolo'];

                        $db->newNotify($_SESSION['utenteID'], $utenteId, 'Cambio ruolo notificato', 'Salve, le è stato cambiato il ruolo nel sito.');
                    }

                ?>
                    <div class="row justify-content-center">
                            <h2  style="padding:1%">
                                <strong>Modifica Utente</strong>
                            </h2>
                    </div> 
                    <label style="display:none" for="id"><?php echo $utenteId?></label>
                    
                    <div class = "row justify-content-center">
                        <div class="col-md-3 text-center">
                            <div class="form-group">
                                <label for="nome" class="text-center">Nome</label>
                                <input type="text" class="form-control" id="nome" name="name" value='<?php if (isset($nome)){ echo $nome;}?>' readonly>
                            </div>
                        </div>

                        <div class="col-md-3 text-center">
                            <div class="form-group">
                                <label for="cognome" class="text-center">Cognome</label>
                                <input type="text" class="form-control" id="cognome" name="surname" value='<?php if (isset($cognome)){ echo $cognome;}?>' readonly>
                            </div>
                        </div>

                        <div class="col-md-3 text-center">
                            <div class="form-group">
                                <label for="form_need" class="text-center">Ruolo</label> 
                                <select id="form_need" name="role" class="form-control" required="required" data-error="" >
                                        <option value="0">0</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                </select>
                            </div>
                        </div>                        
                        <div class="col-md-3 text-center">
                            <label for="cambiaRuolo" class="text-center">Clicca per modificare il ruolo</label>        
                            <button type="submit" class="btn btn-primary btn-block" id="cambiaRuolo" name="editRole" ><span class="fa fa-pencil fa-lg"></span> Modifica</button>
                        </div>                
                    </div>
                </form>
            </div>
        </div> 
    </div>
</div>