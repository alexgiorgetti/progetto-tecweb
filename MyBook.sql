-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Creato il: Gen 27, 2021 alle 11:41
-- Versione del server: 10.4.11-MariaDB
-- Versione PHP: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `MyBook`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `Autore`
--

CREATE TABLE `Autore` (
  `autoreID` int(11) NOT NULL,
  `Nome` text NOT NULL,
  `Cognome` text NOT NULL,
  `AnnoNascita` int(4) NOT NULL,
  `Descrizione` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `Autore`
--

INSERT INTO `Autore` (`autoreID`, `Nome`, `Cognome`, `AnnoNascita`, `Descrizione`) VALUES
(1, 'Ugo', 'Foscolo', 1778, 'Scrittore neoclassicista'),
(2, 'Roberto', 'Savino', 1979, 'Nei suoi scritti, articoli e nel suo romanzo di esordio Gomorra (che lo ha portato alla notorietà) utilizza la letteratura e il reportage per raccontare la realtà economica, di territorio e d\'impresa della Camorra e della criminalità organizzata in senso più generale.'),
(3, 'Italo', 'Calvino', 1923, 'Italo Calvino è stato uno scrittore italiano. Intellettuale di grande impegno politico, civile e culturale, è stato uno dei narratori italiani più importanti del secondo Novecento.'),
(4, 'George', 'Orwell', 1903, 'George Orwell, pseudonimo di Eric Arthur Blair, è stato uno scrittore, giornalista, saggista, attivista e critico letterario britannico.'),
(5, 'Luigi', 'Pirandello', 1867, 'Luigi Pirandello è stato un drammaturgo, scrittore e poeta italiano, insignito del Premio Nobel per la letteratura nel 1934. Per la sua produzione, le tematiche affrontate e l\'innovazione del racconto teatrale è considerato tra i più importanti drammaturghi del XX secolo.');

-- --------------------------------------------------------

--
-- Struttura della tabella `CasaEditrice`
--

CREATE TABLE `CasaEditrice` (
  `casaEditriceID` int(11) NOT NULL,
  `Nome` text NOT NULL,
  `PIVA` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `CasaEditrice`
--

INSERT INTO `CasaEditrice` (`casaEditriceID`, `Nome`, `PIVA`) VALUES
(1, 'Mondadori', '08386600152'),
(2, 'Cairo', '04948640158'),
(3, 'Garzanti', '10283970159'),
(4, 'De Agostini', '02611940038'),
(5, 'Rizzoli', '08856650968');

-- --------------------------------------------------------

--
-- Struttura della tabella `Libro`
--

CREATE TABLE `Libro` (
  `libroID` int(11) NOT NULL,
  `Titolo` text NOT NULL,
  `Prezzo` int(11) NOT NULL,
  `Genere` text NOT NULL,
  `Trama` longtext NOT NULL,
  `Immagine` longtext NOT NULL,
  `casaEditriceID` int(11) NOT NULL,
  `autoreID` int(11) NOT NULL,
  `venditoreID` int(11) NOT NULL,
  `Disponibile` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `Libro`
--

INSERT INTO `Libro` (`libroID`, `Titolo`, `Prezzo`, `Genere`, `Trama`, `Immagine`, `casaEditriceID`, `autoreID`, `venditoreID`, `Disponibile`) VALUES
(1, 'Dei sepolcri', 20, 'Romantico', 'Si tratta di un carme composto da 295 endecasillabi sciolti.', 'img/dei_sepolcri.jpg', 1, 1, 14, 0),
(2, 'Le Grazie', 15, 'Poesia', 'Le Grazie è un poemetto o carme incompiuto, composto nel 1812 da Ugo Foscolo, e dedicato allo scultore Antonio Canova, che in quel momento lavorava al gruppo marmoreo delle Grazie.', 'img/le_grazie.jpg', 5, 1, 14, 0),
(3, 'Gomorra', 50, 'Realistico', 'Il libro è un viaggio nel mondo affaristico e criminale della camorra e dei luoghi dove questa è nata e vive: la Campania, Napoli, Casal di Principe, San Cipriano d\'Aversa, Casapesenna, Mondragone, Giugliano, luoghi dove l\'autore è cresciuto e dei quali fa conoscere al lettore un\'inedita realtà.', 'img/gomorra.jpg', 2, 2, 15, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `Notifica`
--

CREATE TABLE `Notifica` (
  `notificaID` int(11) NOT NULL,
  `Oggetto` text NOT NULL,
  `Messaggio` text NOT NULL,
  `utenteID_S` int(11) NOT NULL,
  `utenteID_D` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `Notifica`
--

INSERT INTO `Notifica` (`notificaID`, `Oggetto`, `Messaggio`, `utenteID_S`, `utenteID_D`) VALUES
(58, 'Conferma nuovo ordine', 'Il tuo ordine in data 2021-01-27 e\' stato processato correttamente.', -2, 16),
(59, 'Conferma nuovo ordine', 'Hai ricevuto un ordine in data 2021-01-27.', -2, 15),
(60, 'Conferma nuovo ordine', 'Il tuo ordine in data 2021-01-27 e\' stato processato correttamente.', -2, 16),
(61, 'Conferma nuovo ordine', 'Hai ricevuto un ordine in data 2021-01-27.', -2, 14),
(62, 'Aggiornamento ordine', 'Il tuo ordine 39 e\' stato Spedito.', 14, 16),
(63, 'Richiesta da contatti: Informazioni prodotto', 'Richiesta da: Alex Giorgetti - alex.giorgetti@hotmail.it</br>Messaggio prova.', -1, 11),
(64, 'Richiesta da contatti: Informazioni prodotto', 'Richiesta da: Alex Giorgetti - alex.giorgetti@hotmail.it</br>Messaggio prova.', -1, 12),
(65, 'Richiesta da contatti: Informazioni prodotto', 'Richiesta da: Alex Giorgetti - alex.giorgetti@hotmail.it</br>Messaggio prova.', -1, 13);

-- --------------------------------------------------------

--
-- Struttura della tabella `Ordine`
--

CREATE TABLE `Ordine` (
  `ordineID` int(11) NOT NULL,
  `LibroOrdinato` longtext NOT NULL,
  `DataOrdine` date NOT NULL,
  `Stato` text NOT NULL,
  `utenteID` int(11) NOT NULL,
  `venditoreID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `Ordine`
--

INSERT INTO `Ordine` (`ordineID`, `LibroOrdinato`, `DataOrdine`, `Stato`, `utenteID`, `venditoreID`) VALUES
(38, '{\"3\":\"2\"}', '2021-01-27', 'Confermato', 16, 15),
(39, '{\"2\":1,\"1\":\"2\"}', '2021-01-27', 'Spedito', 16, 14);

-- --------------------------------------------------------

--
-- Struttura della tabella `Utente`
--

CREATE TABLE `Utente` (
  `utenteID` int(11) NOT NULL,
  `Nome` text NOT NULL,
  `Cognome` text NOT NULL,
  `Username` text NOT NULL,
  `Password` text NOT NULL,
  `Telefono` text NOT NULL,
  `DataNascita` date NOT NULL,
  `IndirizzoResidenza` text NOT NULL,
  `CittaResidenza` text NOT NULL,
  `Ruolo` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `Utente`
--

INSERT INTO `Utente` (`utenteID`, `Nome`, `Cognome`, `Username`, `Password`, `Telefono`, `DataNascita`, `IndirizzoResidenza`, `CittaResidenza`, `Ruolo`) VALUES
(11, 'Alex', 'Giorgetti', 'alex@admin.it', '$2y$10$u04bMvBT2y/VQXwHJg9Ih.Ip3kjTwR5XL.R7y5ZJQdME7C0L2OxZS', '3406277298', '1999-10-06', 'via del Tulipano 25', 'Rimini', 2),
(12, 'Davide', 'Ciandrini', 'davide@admin.it', '$2y$10$re4adpFNe48GlEqEGHR08.iSGQXlJgqI47xL5t7dOMsMD9LRSNdQ2', '3897649876', '1999-01-10', 'Via Roma 1', 'Rimini', 2),
(13, 'Riccardo', 'Albertini', 'riccardo@admin.it', '$2y$10$o5LhAO/1CvcLV/7ga4g8ueo/RsIn7xw0LQgMiBylbA8jvLePtuhoS', '3897649234', '1999-01-10', 'Via dei Martiri 23', 'Rimini', 2),
(14, 'Marco', 'Rossi', 'marco@rossi.it', '$2y$10$28StrOFas6y5uDK5e04xxuNgAOqytPoL2nBGH002qMKyF3ugahN1G', '3987629874', '1998-12-12', 'Via Ubaldo 2', 'Roma', 1),
(15, 'Fabio', 'Mazzini', 'fabio@mazzini.it', '$2y$10$KgEq9FFzCKzsCmsJ2BkRAOkhzks3/3yvlBSNka6qLFjGcb8M/UOWC', '3298757489', '2000-01-01', 'Via Lazio 3', 'Napoli', 1),
(16, 'Andrea', 'Moretti', 'andrea@moretti.it', '$2y$10$IdQB3pEtqDnysQQ3fW5a5ONViCS6asxbJ6MeDsgQchTi826BA9Ja.', '3567849298', '1998-01-01', 'Via Rosti 45', 'Roma', 0);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `Autore`
--
ALTER TABLE `Autore`
  ADD PRIMARY KEY (`autoreID`);

--
-- Indici per le tabelle `CasaEditrice`
--
ALTER TABLE `CasaEditrice`
  ADD PRIMARY KEY (`casaEditriceID`);

--
-- Indici per le tabelle `Libro`
--
ALTER TABLE `Libro`
  ADD PRIMARY KEY (`libroID`);

--
-- Indici per le tabelle `Notifica`
--
ALTER TABLE `Notifica`
  ADD PRIMARY KEY (`notificaID`);

--
-- Indici per le tabelle `Ordine`
--
ALTER TABLE `Ordine`
  ADD PRIMARY KEY (`ordineID`);

--
-- Indici per le tabelle `Utente`
--
ALTER TABLE `Utente`
  ADD UNIQUE KEY `utenteID` (`utenteID`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `Autore`
--
ALTER TABLE `Autore`
  MODIFY `autoreID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT per la tabella `CasaEditrice`
--
ALTER TABLE `CasaEditrice`
  MODIFY `casaEditriceID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT per la tabella `Libro`
--
ALTER TABLE `Libro`
  MODIFY `libroID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT per la tabella `Notifica`
--
ALTER TABLE `Notifica`
  MODIFY `notificaID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT per la tabella `Ordine`
--
ALTER TABLE `Ordine`
  MODIFY `ordineID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT per la tabella `Utente`
--
ALTER TABLE `Utente`
  MODIFY `utenteID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
